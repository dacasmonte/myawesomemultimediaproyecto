# Proyecto Multimedia: Reproductor de música
# VoidPlayer

## Introducción
Este proyecto consiste en la implementación de un reproductor de Multimedia en C#. Este reproductor reproducirá Audio (MP3/FLAC) y Video (MP4/MKV)

## Explicación de la implementación
La aplicación consta de los siguientes atributos:


## Funcionalidades
1. **Cargar directorio:** el usuario puede cargar un directorio con canciones.
2. **Eliminar directorio:** el usuario puede eliminar un directorio seleccionado en la lista de directorios.
3. **Botones multimedia :** controles multimedia esperables de un reproductor. Además, son botones interactivos, en Pausa , Loop y Shuffle
4. **Seleccionar reproducir canción:** se reproducirá la canción sobre la que el usuario haga doble click.
5. **Refresh:** se vuelven a cargar los directorios y canciones.
6. **Volumen ya funcional**: Dependiendo del volumen general del SO puedes subir, bajar o silenciar la reproduccion
7. **Reproduccion tanto de video como de audio** : Dependiendo de las extensiones. Se tienen contempladas las extensiones .mp3 y .flac para audio y .mp4 y .mkv para video, aunque en el futuro se contemplarán más
8. **Carga de Metadatos**: Se cargan los datos del archivo a reproducir en una lista (A nivel de UI todavia puede modificarse y seguramente se haga) para que el usuario los vea. Además , se mostrará si hubiese la portada del disco de la canción
9. **Slider de Progreso** : Que nos permite ver tanto por donde va la reproduccion como saltar a un instante determinado del mismo audio/video
10. **Control de Directorios y Playlist** : Control de archivos accesibles entre directorios, de manera que si borras un directorio borras el acceso a este. Acceso a Playlist obtenido gracias a esto
11. **Busqueda** : Seleccion de cancion/video dependiendo de nombre, autor o album
12. **Cargar directorio independiente**: Reproduccion de carpetas enteras de multimedia sin necesitar seleccionar





## Bibliografia
* [Bibliografia de WPF y C# DE Microsoft](https://docs.microsoft.com/es-es/dotnet/csharp/)
* [Libreria de Metadatos en C++/C#](https://taglib.org/)
