﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Forms;
using Windows.Storage;
using Windows.Storage.FileProperties;
using System.Windows.Threading;
using System.Windows.Input;
using System.Text;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Linq;

namespace WpfApp1
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Atributos
        private static Double OPACIDAD = 0.6;
        private static Double MUL_TAM = 0.9;
        private static string PLAYLISTS= "playlist.txt";
        private static string IMAGENES = Directory.GetCurrentDirectory() + "\\imagenes\\"; //Ruta para busqueda de imagenes 
        private List<string> listaCanciones; //Como su nombre indica , lista de las canciones con su directorios para su reproduccion posterior
        private int cancionActual; //indice de cancion por la que vamos
        private Boolean reproduciendo; //booleano que nos dice si esta reproduciendo o no
        private DispatcherTimer timer; // temporizador para arrastrar el progreso
        private Boolean seleccionado; //Dice si estamos seleccionado el slider para controlar que no pase el tiempo o si (porque no se nos vaya el tiempo)
        private Boolean loopActivated; // Dice si activamos la opcion de repetir canciones , PERO NO SOLO UNA
        private Boolean modoAleatorio; //Activa la reproduccion aleatoria
        private Dictionary<String,List<String>> Directorios; //Guarda las playlist para en caso de borrar una borras las canciones asociadas
        private Dictionary<String, List<String>> Albumes; //Lo mismo pero con metadatos de albumes
        private Dictionary<String, List<String>> Autores; //mismo pero con metadatos de autores
        private enum enumTipoBusqueda {Titulo, Autor, Album}
        private enumTipoBusqueda tbusq = enumTipoBusqueda.Titulo; //Lo usaremos para cambiar entre busqueda por cada elemento del enum
        //Constructor
        public MainWindow()
        {
            InitializeComponent();
            cancionActual = -1;
            listaCanciones = new List<string>();
            Directorios = new Dictionary<string, List<string>>();
            Albumes = new Dictionary<string, List<string>>();
            Autores = new Dictionary<string, List<string>>();
            CargarPlaylist();
            reproduciendo = false;
            seleccionado = false;
            loopActivated = false;
            modoAleatorio = false;
            cuadroBusqueda.Text = "";
            BusqButon.Content = enumTipoBusqueda.Titulo + " " + char.ConvertFromUtf32(0x2193);
            mostrarDirectorios.AllowDrop = true;
            
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1); // Tick se dispara cada segundo.
            timer.Tick += new EventHandler(timer_Tick);
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (!seleccionado) // Si NO hay operación de arrastre en el sliderTimeLine, su posición se actualiza cada segundo.
            {
                progressSlider.Value = visualizacion.Position.TotalSeconds; //en cada segundo actualiza la posicion del slider para ser igual al instante de la multimedia
                contadorTiempo.Content = ((int)visualizacion.Position.TotalMinutes) + ":" + (int)visualizacion.Position.TotalSeconds % 60;
            }
        }

        private void insert_MetaData(int cancionActual) //Añade a la interfaz los metadtos
        { //Acceso e insercion de metadatos en la UI

            visualizacion.Source = (new Uri(listaCanciones[cancionActual]));
            TagLib.File cancion = TagLib.File.Create(listaCanciones[cancionActual]);

            //Limpieza de la lista de Metadatos para que no se acumulen
            metaD.Items.Clear();
            if (cancion.Tag.Title != null)
            {

                //Mostramos los datos de la cancion

                metaD.Items.Add("Titulo : " + cancion.Tag.Title);
                metaD.Items.Add("Compositor/a: " + cancion.Tag.FirstComposer);
                metaD.Items.Add("Artista: " + cancion.Tag.FirstPerformer);
                metaD.Items.Add("Album: " + cancion.Tag.Album);
                metaD.Items.Add("Genero: " + cancion.Tag.FirstGenre);
                metaD.Items.Add("Numero de Pista: " + cancion.Tag.Track);//Numero de pista dentro del álbum

                if (cancion.Tag.Pictures.Length != 0) // Carga de imagen si los metadatos tienen para ponerla de portada
                {
                    //Convierte imagen de metadatos en mapa de bits para poder tratarse

                    TagLib.IPicture pic = cancion.Tag.Pictures[0];
                    MemoryStream ms = new MemoryStream(pic.Data.Data);
                    ms.Seek(0, SeekOrigin.Begin);

                    BitmapImage bp = new BitmapImage();
                    bp.BeginInit();
                    bp.StreamSource = ms;
                    bp.EndInit();
                    imagenAudio.Source = bp;
                }
                else//Imagen por defecto si no tiene caratula
                {
                    string strUri2 = String.Format(IMAGENES + "corchea.png");
                    imagenAudio.Source = new BitmapImage(new Uri(strUri2));
                }


            }
            else//Si NO tiene metadatos, mostramos texto y caratula por defecto
            {
                metaD.Items.Add("El archivo no dispone de metadatos");
                string strUri2 = String.Format(IMAGENES + "corchea.png");
                imagenAudio.Source = new BitmapImage(new Uri(strUri2));
            }

        }
       
   
        //Carga las carpetas del usuario almacenadas
        private void CargarPlaylist()
        {
            Directorios.Clear();
            mostrarCanciones.Items.Clear();
            mostrarDirectorios.Items.Clear();
            listaCanciones.Clear();
            System.IO.StreamReader file = new StreamReader(PLAYLISTS);
            String linea;
            String auxLinea=""; //guardara la linea anterior para meter los datos de directorio
            int cont = 0;
            while ((linea = file.ReadLine()) != null)
            {
                /*Las lineas pares contendran directorios y las impares las canciones guardadas de las lineas pares anteriores a estos . Por tanto, escogemos guardar un auxLinea
                    para así saber en las lineas impares cual es su directorio predeterminado
                */
                if (cont % 2 == 0) //Es directorio
                {
                    auxLinea = linea;

                    if (!Directorios.ContainsKey(linea))//Si no existe ese directorio se añade al map para posteriores introducciones de datos
                    { 
                        Directorios.Add(linea, new List<string>());
                        if(!mostrarDirectorios.Items.Contains(linea))
                            mostrarDirectorios.Items.Add(linea);
                    }
                }
                else //Es la lista de archvos dentro del directorio
                {//convierrte strings a datos para la lista
                    List<string> datosLinea = JsonConvert.DeserializeObject<List<string>>(linea); ;
                    Directorios[auxLinea].AddRange(datosLinea);
                    for(int i = 0; i < datosLinea.Count; i++)
                    {
                        //Añadimos a cada EEDD los datos de la cancion que estamos mirando actualmente , para su posterior uso dependiendo de la seleccion (Titulo,Autor, Album)
                        //las comparaciones se hacen entre minusculas para facilitar 
                        IntroduceDatosAutor(auxLinea + "\\" + datosLinea[i]);
                        IntroduceDatosAlbum(auxLinea + "\\" + datosLinea[i]);
                        listaCanciones.Add(auxLinea + "\\" + datosLinea[i]);
                    }

                }
                cont++;
                
            }
                
            file.Close();
            Busqueda();
        }

        private void IntroduceDatosAutor(String rutaAbsoluta)
        {

            //Introduccion de datos para busqueda por autor. Busca en el mapa con la clave de autor y la lista sin nombre de directorio
            
            TagLib.File cancion = TagLib.File.Create(rutaAbsoluta);   
            if (cancion.Tag.FirstPerformer != null)
            {
                if (Autores.ContainsKey(cancion.Tag.FirstPerformer.ToLower()))
                {
                    if(!Autores[cancion.Tag.FirstPerformer.ToLower()].Contains(System.IO.Path.GetFileName(rutaAbsoluta)))
                        Autores[cancion.Tag.FirstPerformer.ToLower()].Add(System.IO.Path.GetFileName(rutaAbsoluta));
                }
                else
                {
                    Autores.Add(cancion.Tag.FirstPerformer.ToLower(), new List<string>());
                    Autores[cancion.Tag.FirstPerformer.ToLower()].Add(System.IO.Path.GetFileName(rutaAbsoluta));
                }
            }
        }

        private void IntroduceDatosAlbum(String rutaAbsoluta)
        {
            //Introduccion de datos para busqueda por album . Busca en el mapa con la clave de album  y la lista sin nombre de directorio
            TagLib.File cancion = TagLib.File.Create(rutaAbsoluta);
            if (cancion.Tag.Album != null)
            {
                if (Albumes.ContainsKey(cancion.Tag.Album.ToLower()))
                {
                    if(!Albumes[cancion.Tag.Album.ToLower()].Contains(System.IO.Path.GetFileName(rutaAbsoluta)))
                        Albumes[cancion.Tag.Album.ToLower()].Add(System.IO.Path.GetFileName(rutaAbsoluta));
                }
                else
                {
                    Albumes.Add(cancion.Tag.Album.ToLower(), new List<string>());
                    Albumes[cancion.Tag.Album.ToLower()].Add(System.IO.Path.GetFileName(rutaAbsoluta));
                }
            }
        }
        //Almacena la estructura de datos de directorios en un fichero
        private void GuardarPlaylist()
        {  
            FileStream archivo = File.Create(PLAYLISTS);
            archivo.Close();
            System.IO.StreamWriter file = new StreamWriter(PLAYLISTS);

            //pasa de string a json para guardar datos en el fichero txt
            foreach (KeyValuePair<string, List<string>> entry in Directorios)
            {
                file.WriteLine(entry.Key);
                file.WriteLine(JsonConvert.SerializeObject(entry.Value));
            }
            file.Close();
        }

        //Elimina una playlist seleccionada por el usuario
        private void EliminarPlaylist()
        {
            if(mostrarDirectorios.SelectedItem != null)
            {
                EliminaCancionesDirectorio(mostrarDirectorios.SelectedItem.ToString());
                mostrarDirectorios.Items.Remove(mostrarDirectorios.SelectedItem.ToString());  //Tras eliminar las canciones de ese directorio borra el directorio para eliminar sus datos del todo
                GuardarPlaylist();
                CargarPlaylist();

            }
            else//Mensaje de error si no ha seleccionado ningún directorio
            {
                System.Windows.MessageBox.Show("Selecciona al menos una carpeta. Dejará de aparecer en tu lista, pero no será borrada de su ubicación");
            }
            /*Una vez se ha terminado de recargar los datos , se comprueba si hay varias canciones o una. Si hay una la carga por defecto, si hay varias, buscará la cancion siguiente
                Esto se ha hecho así para evitar que siga reproduciendo si sigue en memoria.
            */
            if (mostrarCanciones.Items.Count == 1)
            {
                cancionActual = 0;
                visualizacion.Source = new Uri(listaCanciones[cancionActual]);
                insert_MetaData(cancionActual);
                visualizacion.Play();
            }
            else
            {
                cancionSig();
            }
            
        }

        //Elimina referencias de las canciones de ese directorio
        private void EliminaCancionesDirectorio(String dir)
        {
            
            List<String> aux = Directorios[dir];
            int numVar = aux.Count;
            for (int i=0; i< numVar;i++)
            {
                //Cogemos canciones para borrarlo de las listas de sus metadatos
                TagLib.File cancion = TagLib.File.Create(dir + "\\" + aux[i]);
                //Borramos las canciones de la EEDD de AUTORES
                        if ((cancion.Tag.FirstPerformer != null) && (Autores.ContainsKey(cancion.Tag.FirstPerformer.ToLower()))) //ToLower = Minuscula
                    {
                        for (int j = 0; j < Autores[cancion.Tag.FirstPerformer.ToLower()].Count; j++)
                        {
                            if (Autores[cancion.Tag.FirstPerformer.ToLower()][j] == aux[i])
                            {
                                Autores[cancion.Tag.FirstPerformer.ToLower()].RemoveAt(j);
                            }
                        }
                    }
                //Borramos las canciones de la EEDD de Albumes
                if ((cancion.Tag.Album != null) && (Albumes.ContainsKey(cancion.Tag.Album.ToLower()))) //ToLower = Minuscula
                {
                    for (int j = 0; j < Albumes[cancion.Tag.Album.ToLower()].Count; j++)
                    {
                        if (Albumes[cancion.Tag.Album.ToLower()][j] == aux[i])
                            {
                            Albumes[cancion.Tag.Album.ToLower()].RemoveAt(j);
                        }
                    }
                }
                //Una vez borradas de las EEDD auxiliares las borramos de las EEDD principales (búsqueda e interfaz)

                listaCanciones.Remove(dir +"\\"+ aux[i]); 
                mostrarCanciones.Items.Remove(aux[i]);
            }
            Directorios.Remove(dir);
        }
        //Aunque no haya datos, C# necesita esta funcion internamente para funcionar
        private void lista_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void cancionSig()
        {
            //Primero miramos si hay varias. Si es así, controlamos si hay aleatoriedad seleccionada o no. Dependiendo del resultado, cogeremos el indice siguiente u otro
            if (listaCanciones.Count > 0)
            {
                if (listaCanciones.Count > 1) {
                    if (modoAleatorio)
                    {
                        int ant = cancionActual;
                        do
                        {
                            Random r = new Random();
                            cancionActual = (cancionActual + (r.Next(listaCanciones.Count))) % listaCanciones.Count;
                        } while (cancionActual == ant || cancionActual < 0);

                    }
                    else
                    {
                        cancionActual = (cancionActual + 1) % listaCanciones.Count;
                    }
                    visualizacion.Source = new Uri(listaCanciones[cancionActual]);
                    insert_MetaData(cancionActual);
                    visualizacion.Play();
                }
                else
                {
                    cancionActual = (cancionActual + 1) % listaCanciones.Count;
                    visualizacion.Source = new Uri(listaCanciones[cancionActual]);
                    insert_MetaData(cancionActual);
                    visualizacion.Play();
                }
            }
            else//Estado por defecto si la lista está vacía
            {
                visualizacion.Source = null;
                metaD.Items.Clear();
                string strUri2 = String.Format(IMAGENES + "corchea.png");
                imagenAudio.Source = new BitmapImage(new Uri(strUri2));
            }

            //Una vez seleccionada cancion, acutalizamos la interfaz
            mostrarCanciones.SelectedIndex = cancionActual;
            play.Source = new BitmapImage(new Uri(String.Format(IMAGENES + "pause.png")));
            reproduciendo = true;
        }

        private void cancionAnt()
        {
            if (cancionActual != -1)
            { //Misma explicación que en cancionSig()
                if (modoAleatorio)
                {
                    int ant = cancionActual;
                    do
                    {
                        Random r = new Random();
                        cancionActual = (cancionActual - (r.Next(listaCanciones.Count - 1) )) % listaCanciones.Count;
                    } while (cancionActual == ant || cancionActual == -1);
                    //A veces ha fallado el módulo y ha devuelto número negativos
                    if (cancionActual < -1)
                    {
                        cancionActual = (listaCanciones.Count - 1 - cancionActual) % listaCanciones.Count;
                    }
                }
                else
                {
                    if (cancionActual == 0)
                    {
                        cancionActual = listaCanciones.Count - 1;
                    }
                    else
                    {
                        cancionActual = (cancionActual - 1) % listaCanciones.Count;
                    }
                    
                }
                mostrarCanciones.SelectedIndex = cancionActual;
                visualizacion.Source = new Uri(listaCanciones[cancionActual]);
                insert_MetaData(cancionActual);
                visualizacion.Play();
            }
            
        }

        private void click(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            BotonAdd.Width = BotonAdd.Width * MUL_TAM;

            //Abre dialogo filtrando por el apartado filter
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic); //Esto instancia la carpeta "Musica" de Windows como carpeta inicial
            dialog.Multiselect = true; // Obviamente, permite seleccionar varios archivos para musica
            dialog.Title = "Seleccione solamente archivos de Musica y Video (mp3/mp4/flac/mkv)";
            dialog.Filter = ".mp3 / .flac / .mp4 / .mkv |*.mp3;*.flac;*.wkv;*.mp4|Audio  (*.mp3;*.flac)|*.mp3;*.flac|video (*.wkv;*.mp4)|*.wkv;*.mp4"; //Seleccionamos filtro que nos dará la ventana de Windows a la hora de seleccionar Archivo

            try
            {
                // Estamos mirando si tiene archivos seleccionados el dialogo para poder cargalos

                if (dialog.ShowDialog() != null)
                {
                    visualizacion.Stop();
                    visualizacion.Source = null; //Quitamos referencia alguna cancion para evitar problemas
                    String[] archivos = dialog.FileNames; //Guardamos  todas las canciones
                    agregarElementosRutas(archivos);
                }

            }catch(Exception exec)
            {
                Console.WriteLine(exec.Message);
            }

            GuardarPlaylist();//Se actualizan los datos almacenados
            CargarPlaylist();

        }

        //Dada una lista con rutas de archivos o directorios, los añade a las estrucutras principales
        private void agregarElementosRutas(String[] archivos) {
            //Para los directorios arrastrados , filtra por el arrya de extensiones y escoge de la carpeta solo los archivos con el filtro de extension
            string[] extensiones = {".mp3", ".flac", ".mkv", ".mp4" };
            List<String> reproductor = new List<String>();

            foreach (String arch in archivos)
            {
                if (System.IO.Directory.Exists(arch)) {//SI es directorio, coge los archivos de su interior pero no subdirectorios
                    reproductor.AddRange(Directory.GetFiles(arch, "*.mp3"));
                    reproductor.AddRange(Directory.GetFiles(arch, "*.flac"));
                    reproductor.AddRange(Directory.GetFiles(arch, "*.mkv"));
                    reproductor.AddRange(Directory.GetFiles(arch, "*.mp4"));
                }
                else//Los archivos son filtrados
                {
                    if (extensiones.Contains(Path.GetExtension(arch)))// Coge solo los archivos de audio o video
                    {
                        reproductor.Add(arch);
                    }
                }
            }
            foreach (String arch in reproductor)//Ahora solo quedan archivos de audio o video
            {
                //Creamos su cancion para ver sus metadatos
                TagLib.File cancion = TagLib.File.Create(arch);
                //Metemos en la lista de musica cada archivo seleccionado
                listaCanciones.Add(arch);
                String multimedia = System.IO.Path.GetFileNameWithoutExtension(arch);
                mostrarCanciones.Items.Add(multimedia);
                //Introduccion de datos para busqueda por nombre

                //Si ya está en la EEDD metido el directorio metemos una cancion mas
                if (Directorios.ContainsKey(System.IO.Path.GetDirectoryName(arch)))
                {
                    Directorios[System.IO.Path.GetDirectoryName(arch)].Add(System.IO.Path.GetFileName(arch));
                }
                else
                { //si no, antes de meterla creamos el directorio en la EEDD
                    Directorios.Add(System.IO.Path.GetDirectoryName(arch), new List<string>());
                    Directorios[System.IO.Path.GetDirectoryName(arch)].Add(System.IO.Path.GetFileName(arch));
                    mostrarDirectorios.Items.Add(System.IO.Path.GetDirectoryName(arch));

                }
                //Introduccion de datos para busqueda por autor
                IntroduceDatosAutor(arch);
                //Introduccion de datos para busqueda por album
                IntroduceDatosAlbum(arch);
                
            }
            if (listaCanciones.Count > 0) {//si hay alguna canción, se reproduce una de ellas
                cancionActual = 0;
                insert_MetaData(cancionActual);
                visualizacion.Play();
            }
        }

        //Botón para avanzar
        private void clickForward(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            forward.Width = forward.Width * MUL_TAM;
            cancionSig();
        }

        //Botón para retroceder
        private void clickRewind(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            rewind.Width = rewind.Width * MUL_TAM;
            if (progressSlider.Value > 5) //Si la cancion lleva mas de 5 segundos de reproducción al pulsar atras estaremos repitiendo la cancion
            {
                progressSlider.Value = 0;
                visualizacion.Position = TimeSpan.FromSeconds(progressSlider.Value);// La posición del MediaElement se actualiza para que coincida con el progreso del sliderTimeLine.
            }
            else //En caso contrario vamos para atrás
            {
                cancionAnt();
            }
            
        }

        //Botón de Stop
        private void cliclStop(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            stop.Width = stop.Width * (MUL_TAM);
            visualizacion.Stop();
            progressSlider.Value = 0;
            reproduciendo = false; // Como hemos dejado de reproducir paramos 
            play.Source = new BitmapImage(new Uri(String.Format(IMAGENES + "play.png"))); //Volvemos a poner play en su valor, al no estar pausado

        }
        private void clickPlay(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            //Cambia tamaño para dar enfasis al uuario
            play.Width = play.Width * MUL_TAM;

            if (mostrarCanciones.SelectedIndex < 0 || mostrarCanciones.SelectedIndex == cancionActual)
            {// si no hay ninguna seleccionada o se selecciona la que se esta reproduciendo
                if (reproduciendo)
                {
                    play.Source = new BitmapImage(new Uri(String.Format(IMAGENES + "play.png")));
                    reproduciendo = false;
 
                    visualizacion.Pause();
                }
                else
                {
                    play.Source = new BitmapImage(new Uri(String.Format(IMAGENES + "pause.png")));
                    
                    visualizacion.Play();
                    reproduciendo = true;
                    
                }
                    
            }
            else
            {//hay que cargar una nueva
                cancionActual = mostrarCanciones.SelectedIndex;
                insert_MetaData(cancionActual);
                progressSlider.Value = 0;
                visualizacion.Play();
                reproduciendo = true;

            }
        }

        private void ListBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Image_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            botonDelete.Width = botonDelete.Width * MUL_TAM;
            EliminarPlaylist();
        }

        private void refresh_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            botonrefresh.Width = botonrefresh.Width * MUL_TAM;
            CargarPlaylist();
        }

        private int BuscaCancion(String texto)
        {
            for (int i = 0; i < listaCanciones.Count; i++)
            {
                //Cogemos el nombre solo para compararlo con el seleccionado
                if (System.IO.Path.GetFileNameWithoutExtension(System.IO.Path.GetFileName(listaCanciones[i])) == texto)
                {
                    return i;
                }
            }
            return -1;//En caso de que no exista
        }
        private void MostrarCanciones_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            //Buscamos la canción  que está seleccionada . Se hace asi para poder buscar en cualquiera de las listas (Titulo/Autor/Album)
            cancionActual = BuscaCancion(mostrarCanciones.SelectedItem.ToString());

            insert_MetaData(cancionActual);
            play.Source = new BitmapImage(new Uri(String.Format(IMAGENES + "pause.png")));
            reproduciendo = true;
            visualizacion.Play();
            progressSlider.Value = 0;
        }

        private void reproductorMediaEnded(object sender, RoutedEventArgs e)
        {
            //Ponemos los datos a 0 de nuevo y si hay loop volvemos a cargarlo, en caso contrario busca la siguiente
            progressSlider.Value = 0;
            if (loopActivated)
            {
                visualizacion.Source = new Uri(listaCanciones[cancionActual]);
                insert_MetaData(cancionActual);
                visualizacion.Play();
            }
            else
            {
                cancionSig();
            }
            
        }

        private void reproductorMediaOpened(object sender, RoutedEventArgs e)
        {
            //Datos por defecto, como posicion del slider o labels de duración de multimedia
            reproduciendo = true;
            if (visualizacion.NaturalDuration.HasTimeSpan)
            {
                TimeSpan ts = visualizacion.NaturalDuration.TimeSpan;
                progressSlider.Maximum = ts.TotalSeconds;
                contadorTiempo.Content = "00:00";
                finalTiempo.Content = ((int)ts.TotalMinutes) + ":" + (int) ts.TotalSeconds%60;
                progressSlider.SmallChange = 1;
            }

            timer.Start();
        }

        //Funciones para la interacción con el slider
        private void SliderTimeLine_DragStarted(object sender, System.Windows.Controls.Primitives.DragStartedEventArgs e)
        {
            seleccionado = true;
        }

        private void SliderTimeLine_DragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        {
            seleccionado = false;
            visualizacion.Position =
                TimeSpan.FromSeconds(progressSlider.Value);// La posición del MediaElement se actualiza para que coincida con el progreso del sliderTimeLine.
        }

        private void SliderTimeLine_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            visualizacion.Position = TimeSpan.FromSeconds(progressSlider.Value);
        }

        //Modo de reproducción cíclica
        private void pressSuffle(object sender, MouseButtonEventArgs e)
        {
            suffleImage.Width = suffleImage.Width * MUL_TAM;

            // Primero cambiamos si activas el aleatorio o no y consecuentemente cambiamos su imagen
            if (modoAleatorio)
            {
                modoAleatorio = false;
                suffleImage.Source = new BitmapImage(new Uri(String.Format(IMAGENES + "shuffle.png")));
            }
            else
            {
                modoAleatorio = true;
                suffleImage.Source = new BitmapImage(new Uri(String.Format(IMAGENES + "shuffleActivated.png")));

            }
        }

        private void pressLoop(object sender, MouseButtonEventArgs e)
        {
            loopImage.Width = loopImage.Width * MUL_TAM;
            // Primero cambiamos si activa el loop o no y consecuentemente cambiamos su imagen
            if (loopActivated)
            {
                loopActivated = !loopActivated;
                loopImage.Source = new BitmapImage(new Uri(String.Format(IMAGENES + "loop.png")));
            }
            else
            {
                loopActivated = !loopActivated;
                loopImage.Source = new BitmapImage(new Uri(String.Format(IMAGENES + "loopActivated.png")));
            }
        }

    //Varias funciones para el feedback  hacia el usuario que controlan el cambio de opacidad y de tamaño
        private void loopImage_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            loopImage.Opacity = OPACIDAD;
        }

        private void loopImage_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            loopImage.Opacity = 1;
        }

        private void rewind_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            rewind.Opacity = OPACIDAD;
        }

        private void rewind_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            rewind.Opacity = 1;
        }

        private void stop_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            stop.Opacity = OPACIDAD;
        }

        private void stop_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            stop.Opacity = 1;
        }

        private void play_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            play.Opacity = OPACIDAD;
        }

        private void play_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            play.Opacity = 1;
        }

        private void forward_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            forward.Opacity = OPACIDAD;
        }

        private void forward_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            forward.Opacity = 1;
        }

        private void suffleImage_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            suffleImage.Opacity = OPACIDAD;
        }

        private void suffleImage_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            suffleImage.Opacity = 1;
        }

        private void loopImage_MouseUp(object sender, MouseButtonEventArgs e)
        {
            loopImage.Width = loopImage.Width / (MUL_TAM);
        }

        private void rewind_MouseUp(object sender, MouseButtonEventArgs e)
        {
            rewind.Width = rewind.Width / (MUL_TAM);
        }

        private void stop_MouseUp(object sender, MouseButtonEventArgs e)
        {
            stop.Width = stop.Width / (MUL_TAM);
        }

        private void play_MouseUp(object sender, MouseButtonEventArgs e)
        {
            play.Width = play.Width / MUL_TAM;
        }

        private void forward_MouseUp(object sender, MouseButtonEventArgs e)
        {
            forward.Width = forward.Width / MUL_TAM;
        }

        private void suffleImage_MouseUp(object sender, MouseButtonEventArgs e)
        {
            suffleImage.Width = suffleImage.Width / MUL_TAM;
        }

        private void BotonAdd_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            BotonAdd.Opacity = OPACIDAD;
        }

        private void BotonAdd_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            BotonAdd.Opacity = 1;
        }

        private void botonDelete_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            botonDelete.Opacity = OPACIDAD;
        }

        private void botonDelete_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            botonDelete.Opacity = 1;
        }

        private void botonrefresh_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            botonrefresh.Opacity = OPACIDAD;
        }

        private void botonrefresh_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            botonrefresh.Opacity = 1;
        }

        private void cuadroBusqueda_TextChanged(object sender, TextChangedEventArgs e)
        {
            Busqueda();
        }

        private void Busqueda() {
            if (listaCanciones != null)
            {
                /*
                Primero eliminamos la lista mostrarCanciones para rellenarla con los nuevos parametros .
                
                Si en el recuadro no hay nada escrito , se mostrará la lista entera. En caso contrario:

                    1- Mira que filtro se va a usar con tbusq
                    2- Dependiendo de ello accede a una EEDD u otra
                    3- En ella corta el texto para que solo tengamos el nombre de la cancion (quitando extension)
                    4- Comparando el texto de busqueda pasado a minisculas con los textos de la EEDD pasado a miniscula, si coincide los muestra en mostrarCanciones
                */
                mostrarCanciones.Items.Clear();
                if (cuadroBusqueda.Text == "")//Dejando el cuadro en blanco se muestra la lista completa
                {
                    mostrarCanciones.Items.Clear();
                    foreach (KeyValuePair<string, List<string>> it in Directorios)
                    {
                        foreach (String str in it.Value)
                        {
                            mostrarCanciones.Items.Add(System.IO.Path.GetFileNameWithoutExtension(str));
                        }
                    }
                }
                else
                {
                    switch (tbusq) 
                    {
                        case enumTipoBusqueda.Titulo:
                            foreach (String it in listaCanciones)
                            {
                                if (System.IO.Path.GetFileNameWithoutExtension(it).ToLower().Contains(cuadroBusqueda.Text.ToLower()))
                                {
                                    mostrarCanciones.Items.Add(System.IO.Path.GetFileNameWithoutExtension(it));
                                }
                            }
                            break;
                        case enumTipoBusqueda.Album:
                            foreach (KeyValuePair<string, List<string>> it in Albumes) {
                                if (it.Key.ToLower().Contains(cuadroBusqueda.Text.ToLower())) {
                                    for(int i = 0; i < it.Value.Count; i++)
                                    {
                                        mostrarCanciones.Items.Add(System.IO.Path.GetFileNameWithoutExtension(it.Value[i]));
                                    }
                                    
                                }
                            }
                            break;
                        case enumTipoBusqueda.Autor:
                            foreach (KeyValuePair<string, List<string>> it in Autores)
                            {
                                if (it.Key.ToLower().Contains(cuadroBusqueda.Text.ToLower()))
                                {
                                    for (int i = 0; i < it.Value.Count; i++)
                                    {
                                        mostrarCanciones.Items.Add(System.IO.Path.GetFileNameWithoutExtension(it.Value[i]));
                                    }
                                }
                            }
                            break;
                    }
                    if (mostrarCanciones.Items.Count == 0)//Si no se ha encontrado nada
                    {
                        mostrarCanciones.Items.Add("Sin coincidencias...");
                    }
                }
            }
        }

        //Varias funciones para el menú contextual y la elección del filtro de búsqueda
        private void BusqButon_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.Button boton = sender as System.Windows.Controls.Button;
            System.Windows.Controls.ContextMenu contextMenu = boton.ContextMenu;
            contextMenu.PlacementTarget = boton;
            contextMenu.IsOpen = true;
            e.Handled = true;
        }

        private void itemTitulo_Click(object sender, RoutedEventArgs e)
        {
            tbusq = enumTipoBusqueda.Titulo;
            BusqButon.Content = enumTipoBusqueda.Titulo + " " +  char.ConvertFromUtf32(0x2193);
            Busqueda();
        }

        private void itemAutor_Click(object sender, RoutedEventArgs e)
        {
            tbusq = enumTipoBusqueda.Autor;
            BusqButon.Content = enumTipoBusqueda.Autor + " " + char.ConvertFromUtf32(0x2193);
            Busqueda();

        }

        private void itemAlbum_Click(object sender, RoutedEventArgs e)
        {
            tbusq = enumTipoBusqueda.Album;
            BusqButon.Content = enumTipoBusqueda.Album + " " + char.ConvertFromUtf32(0x2193);
            Busqueda();

        }

        //Esta función gestiona cuando el usuario sueltra archivos sobre el cuadro de directorios para añadirlos
        private void directorios_Drop(object sender, System.Windows.DragEventArgs e)
        {
            if (e.Data.GetDataPresent(System.Windows.DataFormats.FileDrop)) {
                agregarElementosRutas((string[])e.Data.GetData(System.Windows.DataFormats.FileDrop));
            }
        }
    }
}
